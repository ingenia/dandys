<body style="font-size: 13px;">
	<img src="vendor/img/baner.jpg" width="100%">
	<table style="width: 100%;font-size: 14px;margin-top: 30px;" cellspacing="20">
		<tr>
			<td style="text-align: right;width: 20%;">Nombre:</td>
			<td colspan="3" style="border-bottom: 1px solid #000;">{{$data->nombre}}</td>
		</tr>
		<tr>
			<td style="text-align: right;">Domicilio:</td>
			<td colspan="3"  style="border-bottom: 1px solid #000;">{{$data->direccion}}</td>
		</tr>
		<tr>
			<td style="text-align: right;">Telefono:</td>
			<td colspan="3" style="border-bottom: 1px solid #000;">{{$data->telefono}}</td>
		</tr>
		<tr>
			<td style="text-align: right;">Saco:</td>
			<td style="border-bottom: 1px solid #000;width: 40%;">{{$data->saco}}</td>
			<td style="width: 16%;">Modelo:</td>
			<td style="border-bottom: 1px solid #000;">{{$data->modeloSaco}}</td>
		</tr>
		<tr>
			<td style="text-align: right;">Pantal&oacute;n:</td>
			<td style="border-bottom: 1px solid #000;width: 40%;">{{$data->pantalon}}</td>
			<td>Largo:</td>
			<td style="border-bottom: 1px solid #000;">{{$data->largoPantalon}}</td>
		</tr>
		<tr>
			<td style="text-align: right;">Camisa:</td>
			<td style="border-bottom: 1px solid #000;width: 40%;">{{$data->camisa}}</td>
			<td>Manga:</td>
			<td style="border-bottom: 1px solid #000;">{{$data->camisaManga}}</td>
		</tr>
		<tr>
			<td style="text-align: right;">Faj&iacute;n:</td>
			<td style="border-bottom: 1px solid #000;width: 40%;">{{$data->fajin}}</td>
			<td>Fecha Entrega:</td>
			<td style="border-bottom: 1px solid #000;">{{$data->fecha_entrega}}</td>
		</tr>
		<tr>
			<td colspan="4">Cantidad con Letra ({{ $data->cant_letras }})</td>
		</tr>
		<tr style="padding-top: 20px;font-size: 16px;">
			<td style="text-align: right;" colspan="3">
				A cuenta $
			</td>
			<td>
				{{ number_format($data->acuenta, 2) }}
			</td>
		</tr>
		<tr style="font-size: 16px;">
			<td style="text-align: right;" colspan="3">
				Resta $
			</td>
			<td>
				{{ number_format($data->resta, 2) }}
			</td>
		</tr>
		<tr style="font-size: 16px;">
			<td style="text-align: right;" colspan="3">
				Total $
			</td>
			<td>
				{{ number_format($data->total, 2) }}
			</td>
		</tr>
	</table>
<h3>CONTRATO <span style="color:red;margin-left: 100px;"> N° {{ $data->id }} </span></h3>
<p style="background-color: #d6d6d6;height: 30px;"></p>
<p>
CONTRATO DE PRESTACIONES DE SERVICIOS DE RENTA DE TRAJES Y ROPA EN GENERAL DE VESTIR PARA EVENTOS QUE CELEBRAN POR UNA PARTE LA SEÑORA CONSUELO SILVA VALENCIA, A QUIEN EN LO SUCESIVO SE LE DENOMINARÁ “EL PRESTADOR DE SERVICIO” Y POR LA OTRA <b>{{ strtoupper($data->nombre) }}</b> AQUIEN EN LO SUCESIVO SE LE DENOMINARA “EL CONSUMIDOR”, AL TENOR DE LAS SIGUIENTES DECLARACIONES Y CLÁUSULAS.
</p>
<p>
1.- Declara “EL PRESTADOR DE SERVICIO”.
</p>
<p>
a) Ser una persona física de nacionalidad mexicana y quien se identifica con credencial de elector expedida por el Instituto Federal Electoral con Numero SLVLCN61022720M200. <br>
b) Que se encuentra inscrita en el Registro Federal de Contribuyentes con registro SIVC61022BU4.<br>
c) Que cuenta con capacidad, infraestructura, servicios y recursos necesarios para dar cabal cumplimiento a las obligaciones que por virtud del presente contrato adquiere.<br>
d) Que el presente contrato está debidamente inscrito en el Registro Público de contratos de Adhesión de la Procuraduría Federal del Consumidor, en el cumplimiento a lo que establece la NORMA OFICIAL MEXICANA NOM - 111 -  SCF |- 1995. Prácticas Comerciales – Elementos Informativos y requisitos para la contratación de servicios para eventos sociales.<br>
</p>
<p>
Que para los efectos del presente contrato señala como su domicilio en la calle de Jazmines número 216, col. Reforma de esta Ciudad de Oaxaca.
</p>

<br>
<p>
2.- Declara “EL CONSUMIDOR” 
</p>
<p>
a) Llamarse cómo ha quedado plasmado en el proemio de este contrato. <br>
b) Que es su deseo obligarse en los términos y condiciones del presente contrato, manifestando que cuenta con la capacidad legal para la celebración de este acto. <br>
c) Que su número telefónico es el {{ $data->telefono }} <br>
d) Que para los efectos legales de este contrato señala como su domicilio el ubicado en
{{ $data->direccion }}
</p>
<p>
CLAUSULAS:
</p>
<p>
PRIMERA.- El objeto del presente contrato es la prestación de servicios de renta de trajes y todo tipo de ropa para eventos sociales para _____ personas, el cual se llevará a cabo el día {{ $data->dia_evento }} del mes {{ $data->mes_evento }} de {{ date('Y') }} y la renta tendrá una duración de 24 horas, con un costo de $ {{ $data->total }} y la renta de traje y/o ropa será de acuerdo a las características y especificaciones de la carátula de este contrato. <br>
SEGUNDA.- El evento social se llevará a cabo el día {{ $data->dia_evento }} del mes {{ $data->mes_evento }}  de {{ $data->anio_evento }} y tendrá verificativo en {{ $data->lugar_evento }} <br>
“EL CONSUMIDOR” tendrá que depositar como garantía de la renta del traje o ropa del evento su credencial de elector o la de su tutoro representante. <br>
“EL PRESTADOR DEL SERVICIO” podrá cobrar una cantidad adicional, debidamente prevista en el presupuesto, en el caso de que el consumidor prolongue su duración y/o el día señalado para su entrega exceda del estipulado. <br>
TERCERA.- El costo total que el “EL CONSUMIDOR” debe solventar por la prestación del servicio es el estipulado en la cláusula primera del presente contrato. Dicho costo será cubierto por “EL CONSUMIDOR” de contado, en moneda nacional y en forma siguiente: <br>
a) El _______% a la firma del presente contrato, por concepto de anticipo. <br>
b) El restante _________&____________ el día de la entrega del traje o la ropa para el evento. <br>
En caso de que “EL CONSUMIDOR” efectúe un anticipo por los servicios de renta contratados,. “EL PRESTADOR DEL SERVICIO” deberá expedir un comprobante respectivo el que contendrá por lo menos la siguiente información: nombre, razón social, fecha e importe del anticipo, nombre y firma de la persona debidamente autorizada quien recibe el anticipo y el sello del establecimiento, el nombre de “EL CONSUMIDOR”, la fecha, hora y tipo de evento. <br><br>

“EL CONSUMIDOR” se obliga a depositar una cantidad equivalente al 10% del costo estipulado para garantizar el pago de los servicios excedentes, imprevistos o daños o perjuicios en su caso. Dicho depósito será devuelto a “EL CONSUMIDOR” si al finalizar el evento no se verifico ninguno de estos supuestos. <br><br>

Independientemente de la entrega o no del anticipo, “EL PRESTADOR DEL SERVICIO” deberá entregar a “EL CONSUMIDOR” la factura o comprobante que ampare el pago de los servicios contratados, en la que se hará constar detalladamente el nombre y precio de cada uno de los servicios proporcionados, esto con la finalidad de que “EL CONSUMIDOR” pueda verificarlos en detalle. <br><br>

CUARTA.- “EL CONSUMIDOR” cuenta con un plazo de dos días hábiles posteriores a la firma del presente contrato para cancelar la operación sin responsabilidad alguna de su parte en cuyo caso “EL PRESTADOR DE SERVICIO” se obliga a reintegrar todas las cantidades que “EL CONSUMIDOR” le haya entregado, lo anterior no será aplicable si la fecha de la prestación del servicio se encuentra a tres días hábiles o menos de la celebración.<br><br>

QUINTA.- “EL CONSUMIDOR” si es menor de edad tendrá que mencionar quien es su representante o tutor, quien responderá por el tiempo que dure el presente contrato por las obligaciones contraídas.<br><br>

SEXTA.- En su caso “EL CONSUMIDOR” y/o su representante o tutor se obliga a cumplir con las disposiciones reglamentarias que rijan los servicios de renta de traje o ropa para el evento y a procurar el cuidado del traje o ropa, motivo del presente contrato. Para tal efecto “EL PRESTADOR DEL SERVICIO” deberá entregar a “EL CONSUMIDOR” una copia del reglamento respectivo o bien fijar en un lugar visible sus disposiciones.<br><br>

SÉPTIMA.- En caso de que “EL PRESTADOR DEL SERVICO” incurra en incumplimiento del presente contrato por causas imputables a él se obliga a elección de “EL CONSUMIDOR”.<br><br>

a) A contratar por su cuenta a otra empresa que este en posibilidades de realizar la prestación del servicio en las condiciones estipuladas por “EL CONSUMIDOR”. El costo adicional, que en su caso, se genera será absorbido por “EL PRESTADOR DEL SERVICO”, o bien 
b) A reintegrar las cantidades que se le hubieran entregado y a pagar una pena convencional del 20% del costo total del servicio.   <br><br>
 
OCTAVA.- Si los bienes destinados a la presentación del servicio sufrieran un menoscabo por culpa o negligencia debidamente comprobada de “EL CONSUMIDOR” este se obliga a cubrir los gastos de representación de los mismos, o en caso, indemnizar a “EL PRESTADOR DE SERVICIO” hasta con un 60% de su valor.<br><br>

NOVENA.- Las partes podrán aprobar que “EL CONSUMIDOR” contrate libremente servicios específicos relacionados con el evento social con otros prestadores de servicios por así convenir a sus intereses por lo que en caso de actualizarse dicho supuesto “EL CONSUMIDOR” exime de toda responsabilidad a “EL PRESTADOR DEL SERVICIO” en lo que respecta a dichos servicios en específico.<br><br>

DÉCIMA.- En el caso de que “EL PRESTADOR DE SERVICIO” se encuentre imposibilitado, para prestar el servicio por caso fortuito o fuerza mayor, como incendio, temblor u otros acontecimientos de la naturaleza o hechos del hombre ajenos a la voluntad de “EL PRESTADOR DEL SERVICIO”, no se incurrirá en incumplimiento, por lo que no habrá pena convencional en dichos supuestos debiendo únicamente “EL PRESTADOR DEL SERVICIO” reintegrar a “EL CONSUMIDOR” el anticipo que le hubiera entregado.<br><br>

DÉCIMA PRIMERA.- Si “EL CONSUMIDOR” cancela el presente contrato después de los días hábiles que prevé la cláusula quinta “EL CONSUMIDOR” deberá indemnizar a “EL PRESTADOR DEL SERVICIO” por los gastos comprobables hasta por un 20% del costo total de la operación que hubiese realizado en ejecución de la presentación del servicio.<br><br>

DÉCIMA SEGUNDA.- Para la interpretación y cumplimiento del presente contrato las partes se someten a la competencia de la Procuraduría Federal del Consumidor y a las Leyes y Jurisdicción de los Tribunales del Fuero Común.<br><br>

Leído que fue el presente documento y enteradas las partes de su alcance y contenido legal, los suscriben en la Ciudad de Oaxaca, Oaxaca a los {{ date('d') }} Días del Mes de {{ date('M') }} de {{ date('Y') }}.
</p>

<table style="width: 100%;margin-top: 30px;">
	<tr>
		<td style="text-align: center;">
		"EL PRESTADOR DEL SERVICIO" <br>
		CONSUELO SILVA VALENCIA<br>
		</td>
		<td style="text-align: center;">
			"EL CONSUMIDOR". <br>
			{{ strtoupper($data->nombre) }}
		</td>
	</tr>
</table>
</body>
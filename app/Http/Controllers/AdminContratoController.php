<?php 
namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use Mail;
use Hash;
use Cache;
use Validator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\PDF;

class AdminContratoController extends \crocodicstudio\crudbooster\controllers\CBController {

    public function __construct() {
        $this->table              = "contrato";
        $this->primary_key        = "id";
        $this->title_field        = "id";
        $this->limit              = 20;
        $this->index_orderby      = ["id"=>"desc"];
        $this->button_show_data   = true;        
        $this->button_new_data    = true;
        $this->button_delete_data = true;
        $this->button_sort_filter = true;        
        $this->button_export_data = true;
        $this->button_table_action = true;
        $this->button_import_data = false;

        $this->col = array();
        $this->col[] = array("label"=>"Nombre","name"=>"nombre" );
        $this->col[] = array("label"=>"Telefono","name"=>"telefono" );
        $this->col[] = array("label"=>"Direccion","name"=>"direccion" );
        $this->col[] = array("label"=>"Fecha Entrega","name"=>"fecha_entrega" );
        $this->col[] = array("label"=>"Lugar Evento","name"=>"lugar_evento" );

        $this->form = array();
        $this->form[] = array("label"=>"Nombre","name"=>"nombre","type"=>"text","required"=>TRUE,"validation"=>"required|min:3|max:255");
        $this->form[] = array("label"=>"Telefono","name"=>"telefono","type"=>"text","required"=>TRUE,"validation"=>"required|min:3|max:255");
        $this->form[] = array("label"=>"Direccion","name"=>"direccion","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:5|max:255");
        $this->form[] = array("label"=>"Saco","name"=>"saco","type"=>"text","validation"=>"string|min:1|max:255");
        $this->form[] = array("label"=>"Modelo","name"=>"modelo","type"=>"text","validation"=>"string|min:5|max:255");
        $this->form[] = array("label"=>"Pantalon","name"=>"pantalon","type"=>"text","validation"=>"string|min:1|max:255");
        $this->form[] = array("label"=>"LargoPantalon","name"=>"largoPantalon","type"=>"text","validation"=>"string|min:5|max:255");
        $this->form[] = array("label"=>"Camisa","name"=>"camisa","type"=>"text","validation"=>"string|min:1|max:255");
        $this->form[] = array("label"=>"CamisaManga","name"=>"camisaManga","type"=>"text","validation"=>"string|min:5|max:255");
        $this->form[] = array("label"=>"Fajin","name"=>"fajin","type"=>"text","validation"=>"string|min:1|max:255");
        $this->form[] = array("label"=>"Fecha Entrega","name"=>"fecha_entrega","type"=>"date","required"=>TRUE,"validation"=>"required|date");
        $this->form[] = array("label"=>"Fecha Evento","name"=>"fecha_evento","type"=>"date","required"=>TRUE,"validation"=>"required|date");
        $this->form[] = array("label"=>"Lugar Evento","name"=>"lugar_evento","type"=>"text","required"=>TRUE,"validation"=>"required|min:5|max:255");
        $this->form[] = array("label"=>"Acuenta","name"=>"acuenta","type"=>"money","required"=>TRUE,"validation"=>"required|min:2|max:255");
        $this->form[] = array("label"=>"Total","name"=>"total","type"=>"money","required"=>TRUE,"validation"=>"required|min:2|max:255");
     

        /* 
        | ---------------------------------------------------------------------- 
        | Add relational module
        | ----------------------------------------------------------------------     
        | @label       = Name of sub module 
        | @path        = The path of module, see at module generator
        | @foreign_key = required.  
        | 
        */
        $this->sub_module     = array();




        /* 
        | ---------------------------------------------------------------------- 
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------     
        | @label       = Label of action 
        | @route       = URL , you can use alias to get field, prefix [, suffix ], 
        |                e.g : [id], [name], [title], etc ...
        | @icon        = font awesome class icon         
        | 
        */
        $this->addaction = array(['label' => 'Contrato', 'icon' => 'fa fa-file-pdf-o', 'route' => url('admin/contrato/pdf/[id]'), 'blank' => true ]);




                
        /* 
        | ---------------------------------------------------------------------- 
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------     
        | @message = Text of message 
        | @type    = warning,success,danger,info        
        | 
        */
        $this->alert        = array();
                

        
        /* 
        | ---------------------------------------------------------------------- 
        | Add more button to header button 
        | ----------------------------------------------------------------------     
        | @label = Name of button 
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        | 
        */
        $this->index_button = array();       


        
        /* 
        | ---------------------------------------------------------------------- 
        | Add element to form at bottom 
        | ----------------------------------------------------------------------     
        | push your html / code in object array         
        | 
        */
        $this->form_add     = array();       



        
        /*
        | ---------------------------------------------------------------------- 
        | You may use this bellow array to add statistic at dashboard 
        | ---------------------------------------------------------------------- 
        | @label, @count, @icon, @color 
        |
        */
        $this->index_statistic = array();




        /*
        | ---------------------------------------------------------------------- 
        | Add additional view at top or bottom of index 
        | ---------------------------------------------------------------------- 
        | @view = view location 
        | @data = data array for view 
        |
        */
        $this->index_additional_view = array();



        /*
        | ---------------------------------------------------------------------- 
        | Add javascript at body 
        | ---------------------------------------------------------------------- 
        | javascript code in the variable 
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;



        /*
        | ---------------------------------------------------------------------- 
        | Include Javascript File 
        | ---------------------------------------------------------------------- 
        | URL of your javascript each array 
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();



        //No need chanage this constructor
        $this->constructor();
    }


    /*
    | ---------------------------------------------------------------------- 
    | Hook for manipulate query of index result 
    | ---------------------------------------------------------------------- 
    | @query = current database query 
    |
    */
    public function hook_query_index(&$query) {
        //Your code here
            
    }

    /*
    | ---------------------------------------------------------------------- 
    | Hook for manipulate row of index table html 
    | ---------------------------------------------------------------------- 
    | @html for row html 
    | @data for get data row
    | You should using foreach
    |
    */    
    public function hook_row_index(&$html,$data) {
        //Your code here

    }

    /*
    | ---------------------------------------------------------------------- 
    | Hook for manipulate data input before add data is execute
    | ---------------------------------------------------------------------- 
    | @arr
    |
    */
    public function hook_before_add(&$arr) {           

    }

    /* 
    | ---------------------------------------------------------------------- 
    | Hook for execute command after add function called 
    | ---------------------------------------------------------------------- 
    | @id = last insert id
    | 
    */
    public function hook_after_add($id) {  
         
    }

    /* 
    | ---------------------------------------------------------------------- 
    | Hook for manipulate data input before update data is execute
    | ---------------------------------------------------------------------- 
    | @postdata = input post data 
    | @id       = current id 
    | 
    */
    public function hook_before_edit(&$postdata,$id) {        
        //Your code here
    }

    /* 
    | ---------------------------------------------------------------------- 
    | Hook for execute command after edit function called
    | ----------------------------------------------------------------------     
    | @id       = current id 
    | 
    */
    public function hook_after_edit($id) {
         
    }

    /* 
    | ---------------------------------------------------------------------- 
    | Hook for execute command before delete function called
    | ----------------------------------------------------------------------     
    | @id       = current id 
    | 
    */
    public function hook_before_delete($id) {
        //Your code here

    }

    /* 
    | ---------------------------------------------------------------------- 
    | Hook for execute command after delete function called
    | ----------------------------------------------------------------------     
    | @id       = current id 
    | 
    */
    public function hook_after_delete($id) {
        //Your code here

    }

    public function getPdf($id){
        $row = DB::table($this->table)->where('id',$id)->first();  
        $evento = explode("-", $row->fecha_evento);
        $row->dia_evento = $evento[2];
        $row->mes_evento = $evento[1];
        $row->anio_evento = $evento[0];
        $convert = new \App\Libraries\EnLetras();
        $row->cant_letras = $convert->convert($row->total);
        $row->resta = $row->total-$row->acuenta;
        $view = view('contrato',['data' => $row])->render();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('Legal');
        return $pdf->stream('Contrato'.$id.'.pdf');
    }



    //By the way, you can still create your own method in here... :) 


}